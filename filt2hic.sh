#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=8
#SBATCH --job-name="filt2hic"
#SBATCH --output=filt2hic-%j.log
#SBATCH --export=ALL

set -o xtrace

filtered=$1
name=${filtered%%.*}
hic=${name}.hic
fai=$2
pre=${name}.pre.tsv


zcat $filtered | awk 'BEGIN{OFS="\t";strand["+"]=0;strand["-"]=1;} $1 !~ /^#/ {print strand[$6],$2,$3,$10,strand[$7],$4,$5,$13}' >$pre

java -jar $JUICER pre $pre ${hic} ${fai} 

